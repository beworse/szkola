﻿using NUnit.Framework;


namespace SchoolProjectTest
{
    public class TestingSchool
    {
        [Test]
        public void TestAddingStudnets()
        {
            var mySchool = new SchoolProject.MySchool.School();
            int toAdd = 10;
            for(int i=0; i<toAdd; i++)
                mySchool.AddStudnet();

            Assert.AreEqual(mySchool.students.Count, toAdd);
        }

    }
}
