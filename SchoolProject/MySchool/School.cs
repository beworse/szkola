﻿using SchoolProject.MySchool.Matter;
using SchoolProject.MySchool.People;
using System;
using System.Collections.Generic;

namespace SchoolProject.MySchool
{
    public class School
    {
        /// <summary>
        /// Lista dostępndych klass;
        /// </summary>
        public List<Class> classes { get; private set; }
        /// <summary>
        /// Lista uczniow uczeszczajacych do szkoly
        /// </summary>
        public List<Studnet> students { get; private set; }
        /// <summary>
        /// Lista zatrudnionych nauczycieli
        /// </summary>
        public Teachers teachers = new Teachers();


        /// <summary>
        /// Tworzy nowa szkole
        /// </summary>
        public School()
        {
            classes = new List<Class>();
            students = new List<Studnet>();
        }
        /// <summary>
        /// Tworzy nowa szkole
        /// </summary>
        /// <param name="students">lista uczniow</param>
        /// <param name="classes">lista klass</param>
        public School(List<Studnet> students, List<Class> classes)
        {
            this.classes = classes;
            this.students = students;
        }

        public int AddStudnet()
        {
            Studnet s = new Studnet(students.Count);
            students.Add(s);
            return s.id;
        }
        /// <summary>
        /// Dodanie ucznia do klasy
        /// </summary>
        /// <param name="studnetId"></param>
        /// <param name="classId"></param>
        public void AddStudnetToClass(int studnetId, int classId)
        {
            classes[classId].students.Add(studnetId);
        }

        /// <summary>
        /// Utworzenie nowej klasy
        /// </summary>
        /// <returns>Identyfikator klasy</returns>
        public string AddClass()
        {
            Class c = new Class(classes.Count.ToString());
            classes.Add(c);
            return c.ID;
        }
        /// <summary>
        /// Zwaraca numer na liscie danej klasy
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetIDClass(string id)
        {
            int? searchedId = null;

            //poszukiwanie numeru na liscie
            for(int i=0; i< classes.Count; i++)
            {
                if (classes[i].ID == id)
                {
                    searchedId = i;
                    break;
                }
            }

            if (searchedId == null)
                throw new ArgumentException();

            return Convert.ToInt32(searchedId);
        }
        /// <summary>
        /// Dodanie przedmiotu do klasy
        /// </summary>
        /// <param name="classId"></param>
        /// <param name="subject"></param>
        public void AddSubject(int classId, Subject subject)
        {
            Class c = classes[classId];
            c.subjcets.Add(subject);
        }

    }
}
