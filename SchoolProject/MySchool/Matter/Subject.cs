﻿using SchoolProject.MySchool.People;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolProject.MySchool.Matter
{
    /// <summary>
    /// Dostępnę przedmioty nauczania
    /// </summary>
    public enum Faculties
    {
        [Description("Sztuka")] Art,
        [Description("Biologia")] Biology,
        [Description("Chemia")] Chemistry,
        [Description("Angielski")] English,
        [Description("Geografia")] Geography,
        [Description("Niemiecki")] German,
        [Description("Historia")] History,
        [Description("Informatyka")] IT,
        [Description("Matematyka")] Mathematics,
        [Description("Muzyka")] Music,
        [Description("Wychowanie fizyczne")] PhysicalEducation,
        [Description("Fizyka")] Physics,
        [Description("Polski")] Polish
    }

    /// <summary>
    /// Przedmiot nauczania, nie mozna zmieniac godzin i zadan (rozporzadzenie ministra)
    /// </summary>
    public class Subject
    {
        /// <summary>
        /// Liczba godzin 
        /// </summary>
        public uint hours { get; private set; }
        /// <summary>
        /// Liczba zadań
        /// </summary>
        public uint tasks { get; private set; }
        /// <summary>
        /// Typ przedmiotu
        /// </summary>
        public Faculties faculty { get; private set; }
        /// <summary>
        /// Identyfikator nauczyciela
        /// </summary>
        private int teacherId { get; set; }

        /// <summary>
        /// Utworzenie nowego przedmiotu
        /// </summary>
        /// <param name="faculy">Typ przedmiotu</param>
        /// <param name="hours">Liczba godzin</param>
        /// <param name="tasks">Liczba zadań</param>
        /// <param name="teacherId">Identyfikator nauczyciela uczacego</param>
        public Subject(Faculties faculty, uint hours, uint tasks, int teacherId)
        {
            this.faculty = faculty;
            this.hours = hours;
            this.teacherId = teacherId;
        }

        /// <summary>
        /// Ustawienei nauczyciela uczacego przedmiotu
        /// </summary>
        /// <param name="teacher"></param>
        public void SetTeacher(Teacher teacher)
        {
            teacherId = teacher.id;
        }
        /// <summary>
        /// Ustawienei nauczyciela uczacego przedmiotu
        /// </summary>
        /// <param name="teacherId"></param>
        public void SetTeacher(int teacherId)
        {
            this.teacherId = teacherId;
        }
        
        /// <summary>
        /// Ustawione Id nauczucicela
        /// </summary>
        /// <returns></returns>
        public int GetTeacher()
        {
            return teacherId;
        }
    }
}
