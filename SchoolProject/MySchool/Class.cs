﻿using SchoolProject.MySchool.Matter;
using SchoolProject.MySchool.People;
using System.Collections.Generic;

namespace SchoolProject.MySchool
{
    public class Class
    {
        /// <summary>
        /// Unikatowy identyfikator klasy
        /// </summary>
        public string ID { get; private set; }
        /// <summary>
        /// Lista dostepnych przedmiotow (moga) sie powatarzac
        /// </summary>
        public List<Subject> subjcets;
        /// <summary>
        /// Lista dostępnych uczniów
        /// </summary>
        public HashSet<int> students;

        /// <summary>
        /// Tworzy klasę
        /// </summary>
        /// <param name="ID">Identyfikator klasy</param>
        public Class(string ID)
        {
            this.ID = ID;
            subjcets = new List<Subject>();
            students = new HashSet<int>();
        }
        /// <summary>
        /// Tworzy klasę
        /// </summary>
        /// <param name="ID">Identyfikator klasy</param>
        public Class(string ID, List<Subject> subjcets)
        {
            this.ID = ID;
            this.subjcets = subjcets;
            subjcets = new List<Subject>();
            students = new HashSet<int>();
        }
    }
}