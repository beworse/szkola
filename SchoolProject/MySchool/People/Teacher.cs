﻿using System;
using System.Collections.Generic;
using SchoolProject.MySchool.Matter;
using System.ComponentModel;
using System.Linq;

namespace SchoolProject.MySchool.People
{
    public class Teacher
    {
        #region Pola
        /// <summary>
        /// Przedmioty, kotrych moze uczyc nauczyciel
        /// </summary>
        public HashSet<Faculties> faculties { get; private set; }
        /// <summary>
        /// Przedmioty, ktorych moze uczyc nauczyciel
        /// </summary>
        public List<Faculties> facultiesList { get => faculties.ToList(); }
        /// <summary>
        /// Liczba przedmiotow ktorych moze nauczac nauczyciel
        /// </summary>
        public int facultiesCount { get => faculties.Count; }
        /// <summary>
        /// Identyfikator nauczyciela
        /// </summary>
        public int id { get; private set; }
        #endregion

        #region Konstruktor
        /// <summary>
        /// Konstruktor klasy nauczyciel
        /// </summary>
        /// <param name="id">Unikalny identyfikator nauczyciela</param>
        public Teacher(int id)
        {
            this.id = id;
            faculties = new HashSet<Faculties>();
        }
        /// <summary>
        /// Konstruktor klasy nauczyciel
        /// </summary>
        /// <param name="id">Unikalny identyfikator nauczyciela</param>
        /// <param name="faculties">Przedmiot ktorego naucza</param>
        public Teacher(int id, Faculties faculties)
        {
            this.id = id;
            this.faculties = new HashSet<Faculties>();
            this.faculties.Add(faculties);
        }
        /// <summary>
        /// Konstruktor klasy nauczyciel
        /// </summary>
        /// <param name="id">Unikalny identyfikator nauczyciela</param>
        /// <param name="faculties">Przedmioty ktorego naucza</param>
        public Teacher(int id, HashSet<Faculties> faculties)
        {
            this.id = id;

            this.faculties = faculties;
        }
        #endregion

        #region Fakultet(y)
        /// <summary>
        /// Dodanie przedmiotu jakiego moze nauczac nauczyciel
        /// </summary>
        /// <param name="faculties"></param>
        public void FacultyAdd(Faculties faculties)
        {
            this.faculties.Add(faculties);
        }
        /// <summary>
        /// Dodanie przedmiotow jakich moze nauczac nauczyciel
        /// </summary>
        /// <param name="faculties"></param>
        public void FacultiessAdd(HashSet<Faculties> faculties)
        {
            foreach (var subject in faculties)
            {
                this.faculties.Add(subject);
            }
        }

        /// <summary>
        /// Usuniecie przedmiotu kotrego nauczyciel moze nauczac
        /// </summary>
        /// <param name="faculties"></param>
        public void FacultyRemove(Faculties faculties)
        {
            if (this.faculties.Contains(faculties))
            {
                this.faculties.Remove(faculties);
            }
        }
        /// <summary>
        /// Usuniecie przedmiotow kotrych nauczyciel moze nauczac
        /// </summary>
        /// <param name="faculties"></param>
        public void FacultiesRemove(HashSet<Faculties> faculties)
        {
            foreach (var subject in faculties)
            {
                FacultyRemove(subject);
            }
        }

        /// <summary>
        /// Zwraca nazwy przedmiotow, ktorych nauczyciel, moze nauczac
        /// </summary>
        /// <param name="decsription"></param>
        /// <returns></returns>
        public List<string> FacultyGetNames(bool decsription = false)
        {
            List<string> list = new List<string>();
            if (decsription)
            {
                string name;
                foreach (var subject in faculties)
                {
                    Type type = typeof(Faculties);
                    var field = type.GetField(subject.ToString());
                    var attributes = (DescriptionAttribute[])
                            field.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    name = attributes.Length > 0 ?
                                    attributes[0].Description :
                                    string.Empty;

                    if (name != string.Empty)
                    {
                        list.Add(name);
                    }
                    else
                    {
                        throw new ArgumentException();
                    }

                }
            }
            else
            {
                foreach (var subject in faculties)
                {
                    list.Add(Enum.GetName(typeof(Faculties), subject));
                }
            }
            return list;
        }

        /// <summary>
        /// Sprawdza czy dany nauczyciel posiada uprawnienia do nauczania przedmiotu
        /// </summary>
        /// <param name="faculty"></param>
        /// <returns></returns>
        public bool FacultyCheck(Faculties faculty)
        {
            return faculties.Contains(faculty);
        }
        #endregion
    }

    /// <summary>
    /// Zarzadza lista dostepnych nauczycieli (nie ma mozliwosci usuniecia nauczyciela - skarbowka)
    /// </summary>
    public class Teachers
    {
        /// <summary>
        /// Lista wszystkich nauczycieli
        /// </summary>
        public List<Teacher> list { get; private set; }

        public Teachers()
        {
            list = new List<Teacher>();
        }

        /// <summary>
        /// Dodanie nauczyciela
        /// </summary>
        /// <returns>ID</returns>
        public int Add()
        {
            list.Add(new Teacher(list.Count));
            return list.Count;
        }
        /// <summary>
        /// Dodanie nauczyciela
        /// </summary>
        /// <param name="faculty">przedmiot ktorego moze nauczac</param>
        /// <returns>ID</returns>
        public int Add(Faculties faculty)
        {
            list.Add(new Teacher(list.Count, faculty));
            return list.Count;
        }
        /// <summary>
        /// Dodanie nauczyciela
        /// </summary>
        /// <param name="faculties">Lista przedmiotow kotrych moze nauczac</param>
        /// <returns>ID</returns>
        public int Add(HashSet<Faculties> faculties)
        {
            list.Add(new Teacher(list.Count, faculties));
            return list.Count;
        }
        /// <summary>
        /// Dodanie nauczyciela
        /// </summary>
        /// <param name="teacher">Nauczyciel</param>
        /// <returns>ID</returns>
        public int Add(Teacher teacher)
        {
            list.Add(teacher);
            return list.Count;
        }


        /// <summary>
        /// Zwraca numer identyfikujacy nauczyciela
        /// </summary>
        /// <param name="teacher"></param>
        /// <returns></returns>
        public int GetId(Teacher teacher)
        {
            if(!list.Contains(teacher))
            {
                throw new ArgumentException();
            }
            return list.IndexOf(teacher);
        }

        /// <summary>
        /// Modyfikuje danego nauczyciela
        /// </summary>
        /// <param name="teacher"></param>
        public void ModifyTeacher(Teacher teacher)
        {
            if(teacher.id > list.Count)
            {
                throw new ArgumentException();
            }
            list[teacher.id] = teacher;
        }
    }
    
}
