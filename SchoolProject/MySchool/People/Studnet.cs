﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolProject.MySchool.People
{
    public class Studnet
    {
        /// <summary>
        /// Identyfikator studenta;
        /// </summary>
        public int id;
        /// <summary>
        /// Identyfikator klasy do ktorej nalezy;;
        /// </summary>
        public int idClass;

        /// <summary>
        /// Utworzenie studenta
        /// </summary>
        /// <param name="id">unikatowy idenfyfikator</param>
        public Studnet(int id)
        {
            this.id = id;
        }
        /// <summary>
        /// Utworzenie studenta
        /// </summary>
        /// <param name="id">unikatowy idenfyfikator</param>
        /// <param name="idClass">identyfikator klasy</param>
        public Studnet(int id, int idClass)
        {
            this.id = id;
            this.idClass = idClass;
        }
        
        /// <summary>
        /// Uczen zmienia klase
        /// </summary>
        /// <param name="idClass"></param>
        public void ChangeClass(int idClass)
        {
            this.idClass = idClass;
        }


    }
}
