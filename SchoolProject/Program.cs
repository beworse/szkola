﻿using SchoolProject.MySchool;
using SchoolProject.MySchool.Matter;

namespace SchoolProject
{
    class Program
    {
        static void Main(string[] args)
        {
            School mySchool = new School();

        //zatrudnienie nauczycieli
            int t1 = mySchool.teachers.Add();
            int t2 = mySchool.teachers.Add(Faculties.Mathematics);

        //dododanie studentow
            int st1 = mySchool.AddStudnet();
            int st2 = mySchool.AddStudnet();

            // utworzenie klas 
            var tmp = mySchool.AddClass();
            int cl = mySchool.GetIDClass(tmp);

        // przypisanie studentow do klas
            mySchool.AddStudnetToClass(st2, cl);
            mySchool.AddStudnetToClass(st1, cl);

        // dodanie przedmiotow do klass
            mySchool.AddSubject(cl, new Subject(
                                                Faculties.Mathematics,
                                                100,
                                                10,
                                                t2));

            mySchool.AddSubject(cl, new Subject(
                                                Faculties.IT,
                                                100,
                                                10,
                                                t1));




        }
    }
}
